package hr.laplacian.laplas.llama.streams.ratelimit

import akka.event.Logging
import akka.stream.stage._
import akka.stream.{Attributes, FlowShape, Inlet, Outlet}
import hr.laplacian.laplas.redis.RedisService

import scala.concurrent.duration._

class RedisParallelismLimitInFlow[T] private(
    val key: String,
    val limit: Long,
    val interval: FiniteDuration,
    val tokenRefreshRate: FiniteDuration = 1.second
)(
    implicit val redisService: RedisService
) extends GraphStage[FlowShape[T, ElemWithToken[T]]] {
  val out: Outlet[ElemWithToken[T]] = Outlet[ElemWithToken[T]](Logging.simpleName(this) + ".out")
  val in: Inlet[T] = Inlet[T](Logging.simpleName(this) + ".in")

  override def shape: FlowShape[T, ElemWithToken[T]] = FlowShape(in, out)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic =
    new TimerGraphStageLogic(shape) with InHandler with OutHandler {
      private var isFinished = false
      private var isWaiting = false
      private var currentElem: Option[T] = None

      setHandlers(in, out, this)

      override def onPush(): Unit = {
        if (!isWaiting && !isFinished) {
          currentElem = Some(grab(in))
          tryEmit()
        }
      }

      override def onPull(): Unit = {
        if (!hasBeenPulled(in) && !isFinished && !isWaiting) {
          pull(in)
        }
      }

      override protected def onTimer(timerKey: Any): Unit = tryEmit()

      private def tryEmit(): Unit = {
        val tokenOpt = redisService.getRateLimitToken(key, limit, interval)
        if (tokenOpt.isDefined) {
          emitElem(tokenOpt.get)
          isWaiting = false
        } else {
          isWaiting = true
          scheduleOnce(RedisParallelismLimitInFlow.timerKey, tokenRefreshRate)
        }
      }

      protected def emitElem(token : String): Unit = {
        if (isAvailable(out) && currentElem.isDefined) {
          push(out, ElemWithToken(currentElem.get, key, token))
          currentElem = None
        }
        if (isFinished) {
          completeStage()
        }
      }

      override def onUpstreamFinish(): Unit = {
        if (isWaiting) {
          isFinished = true
        } else {
          completeStage()
        }
      }
    }
}

object RedisParallelismLimitInFlow {

  def apply[T](key: String, limit: Long, interval: FiniteDuration, tickRate: FiniteDuration = 1.second)(
    implicit redisService: RedisService): RedisParallelismLimitInFlow[T] =
    new RedisParallelismLimitInFlow[T](key, limit, interval, tickRate)(redisService)

  private val timerKey = "RedisParallelismLimitInFlowRetryTimer"
}

