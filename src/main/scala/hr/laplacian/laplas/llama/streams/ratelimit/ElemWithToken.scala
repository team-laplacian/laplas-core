package hr.laplacian.laplas.llama.streams.ratelimit

case class ElemWithToken[T](
    elem: T,
    key: String,
    token: String
)
