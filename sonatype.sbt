
publishMavenStyle := true

licenses := Seq("MIT" -> url("http://opensource.org/licenses/MIT"))

homepage := Some(url("https://gitlab.com/team-laplacian/laplas-llama"))
scmInfo := Some(
  ScmInfo(
    url("https://gitlab.com/team-laplacian/laplas-llama"),
    "scm:git@gitlab.com:team-laplacian/laplas-llama.git"
  )
)
developers := List(
  Developer(id="mfranic", name="Mario Franic", email="mario@laplacian.hr", url=url("https://gitlab.com/mfranic"))
)
